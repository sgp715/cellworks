class Addresses:

    def __init__(self, sections):
        self.sections = sections

    def get_physical(self, id):

        if id <= 0 or id > self.sections[-1]:
            raise RuntimeError("Illegal address:" + str(id))

        start = 0
        end = 0
        for i, section in enumerate(self.sections):
            end += section
            if id <= end:
                return (i + 1, id - start)
            start += section

        raise RuntimeError("Illegal address:" + str(id))

if __name__ == "__main__":
    sections = [1, 10, 20, 25]
    a = Addresses(sections)
    print(a.get_physical(1))
    print(a.get_physical(10))
    print(a.get_physical(15))
    print(a.get_physical(20))
    print(a.get_physical(23))