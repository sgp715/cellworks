class Dumb:

    def __init__(self, numpixels, brightness=64):

        self.colors = {
            "white": 0xFFFFFF,
            "black": 0x000000,
            "red": 0xFF0000,
            "blue": 0x00FF00,
            "green": 0x0000FF
        }
        self.numpixels = numpixels
        #datapin = 19
        #clockpin = 23
        bitrate = 12000000


    def turn_off(self, loc):
        print("turn off")
        print(" loc: " + str(loc))
        black_hex = self.colors["black"]

    def change_color(self, loc, color):
        print("change color")
        print(" loc: {} color: {} ".format(loc, color))

if __name__ == "__main__":

    # lights = Lights(2)
    # lights.change_color(0, "white")
    # lights.change_color(0, "red")
    # lights.change_color(0, "green")
    # lights.change_color(0, "blue")
    # lights.turn_off(0)

    numleds = 60
    lights = Dumb(numleds)
    for i in range(numleds):
        lights.change_color(i, "white")
    for i in range(numleds):
        lights.change_color(i, "red")
    for i in range(numleds):
        lights.change_color(i, "green")
    for i in range(numleds):
        lights.change_color(i, "blue")
    for i in range(numleds):
        lights.turn_off(i)