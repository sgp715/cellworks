import sys

from dumby import Dumb
USE_LIGHTS = True
try:
    print("Simulating lights!")
    from lights import Lights
except:
    USE_LIGHTS = False
from nodes import Medicine, Gene, Expression, Result
from tree import Tree
from utils import read_json, json_to_graph
from visualize import generate_viz_graph, write_graph_pic

def get_lights(pixels):

    if USE_LIGHTS:
        return Lights(pixels)
    else:
        return Dumb(pixels)

def main(pause=0):

    lights = get_lights(30)
    data = read_json("./cellworks.json")
    adj = json_to_graph(data)


    def find_node(n, adj):
        for c in adj:
            if c.name == n: return c

    t = Tree(adj, lights, True, "main", pause)
    t.reset()
    t.mutate()
    l = find_node("LENALIDOMIDE", adj)
    t.dfs(l)
    a = find_node("AZACITIDINE", adj)
    t.mutate()
    t.dfs(a)
    t.reset()


def test_tree():
    A = Medicine("A", "1","green")
    B = Gene("B", "2","green")
    C = Expression("C", "3", "white", "blue")
    D = Result("D", "4", "green")
    E = Expression("E", "5", "white", "red")
    adj = {A: [B],
           B: [C, E],
           C: [D],
           D: [],
           E: []}
    return (A, adj)

def test_on():

    lights = get_lights(30)
    A, adj = test_tree()
    body = Tree(adj, lights)
    body.reset()

def test():

    lights = get_lights(30)
    A, adj = test_tree()
    body = Tree(adj, lights)
    body.reset()
    body.mutate()
    body.dfs(A)



def test_off():
    lights = get_lights(30)
    A, adj = test_tree()
    body = Tree(adj, lights)
    body.reset()
    body.shut_down()

if __name__ == "__main__":

    if len(sys.argv) != 2:
        raise RuntimeError("Invalid number of input args:" + str(sys.argv))
        exit()
    elif sys.argv[1] == "main":
        print("Starting...")
        # adj = {}
        main()
    elif sys.argv[1] == "test":
        print("Testing...")
        # test_on()
        test()
        # test_off()
    elif sys.argv[1] == "graph":
        data = read_json("./cellworks.json")
        adj = json_to_graph(data)
        g = generate_viz_graph(adj)
        write_graph_pic("cellworks", g)

