
class Node():

    def __init__(self, name, addresses, initial):
        self.name = name
        self.addresses = addresses
        self.initial = initial
        self.color = "black"

    def __str__(self):
        return "name: {}\n    addresses: {}\n"\
                .format(self.name, self.addresses)

    def __eq__(self, othr):
        if othr == None:
            return False
        return ((self.name) == (othr.name))

    def __hash__(self):
        return hash((self.name))

    def heal(self, lights):
        """
        does anything necessary for this node and gets neighbors
        :return: neighbors
        """
        print(self)

    def turn_on(self, lights):
        print("turn on - addresses:")
        for addy in self.addresses:
            print(addy)
            self.color = self.initial
            lights.change_color(addy, self.initial)
        print("\n")

    def turn_off(self, lights):
        print("turn off - addresses:")
        for addy in self.addresses:
            print(addy)
            self.color = "black"
            lights.turn_off(addy)


class Medicine(Node):

    def __init__(self, name, addresses, initial):
        super().__init__(name, addresses, initial)

    def __eq__(self, othr):
        return super().__eq__(othr)

    def __hash__(self):
        return super().__hash__()

    def heal(self, pi):
        super().heal(pi)

class Gene(Node):

    def __init__(self, name, addresses, initial):
        super().__init__(name, addresses, initial)
        self.type = type

    def __eq__(self, othr):
        return super().__eq__(othr)

    def __hash__(self):
        return super().__hash__()

    def heal(self, lights):
        super().heal(lights)

class Expression(Node):

    def __init__(self, name, addresses, initial, secondary):
        super().__init__(name, addresses, initial)
        self.type = type # over/ under i.e. red/blue
        self.secondary = secondary

    def __eq__(self, othr):
        return super().__eq__(othr)

    def __hash__(self):
        return super().__hash__()

    def __str__(self):
        s = super().__str__()
        s += " type:{} \n".format(self.type)
        return s

    def mutate(self, lights):
        print("mutate - addresses:")
        for addy in self.addresses:
            print(addy)
            self.color = self.secondary
            lights.change_color(addy, self.secondary)

    def heal(self, lights):
        super().heal(lights)
        print("heal - addresses:")
        for addy in self.addresses:
            print(addy)
            # if self.color == self.initial:
            # else:
            self.color = self.initial
            lights.change_color(addy, self.initial)
        print("\n")

class Result(Node):

    def __init__(self, name, addresses, initial):
        super().__init__(name, addresses, initial)

    def __eq__(self, othr):
        return super().__eq__(othr)

    def __hash__(self):
        return super().__hash__()

    def heal(self, lights):
        super().heal(lights)
