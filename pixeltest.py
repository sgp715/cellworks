#!/usr/bin/python

import time
from dotstar import Adafruit_DotStar

numpixels = 2

datapin  = 23
clockpin = 24
strip = Adafruit_DotStar(numpixels, 12000000, order='gbr')

strip.begin()
strip.setBrightness(64)

head  = 0
tail  = 1

colors = {
    "white": 0xFFFFFF,
    "black": 0x000000,
    "red": 0xFF0000,
    "blue": 0x00FF00,
    "green": 0x0000FF
}

def turn_off(strip, loc):
    black_hex = colors["black"]
    strip.setPixelColor(loc, black_hex)

def change_color(strip, loc, color):
    turn_off(strip, loc)
    color_hex = colors[color]
    strip.setPixelColor(loc, color_hex)

turn_off(strip, head)
strip.show()
time.sleep(1.0)

change_color(strip, head, "white")
strip.show()
time.sleep(1.0)

change_color(strip, head, "green")
strip.show()
time.sleep(1.0)

change_color(strip, head, "blue")
strip.show()
time.sleep(1.0)

change_color(strip, head, "red")
strip.show()
time.sleep(1.0)

turn_off(strip, head)
strip.show()
