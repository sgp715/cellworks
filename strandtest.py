#!/usr/bin/python

import time
from dotstar import Adafruit_DotStar

numpixels = 2 # Number of LEDs in strip

datapin  = 23
clockpin = 24
strip = Adafruit_DotStar(numpixels, 12000000, order='gbr')

strip.begin()           # Initialize pins for output
strip.setBrightness(64) # Limit brightness to ~1/4 duty cycle

head  = 0               # Index of first 'on' pixel
tail  = 1           # Index of last 'off' pixel
color = 0xFF0000        # 'On' color (starts red)

while True:                              # Loop forever

	strip.setPixelColor(head, color) # Turn on 'head' pixel
	strip.setPixelColor(tail, 0)     # Turn off 'tail'
	strip.show()                     # Refresh strip
	time.sleep(1.0)             # Pause 20 milliseconds (~50 fps)

	head += 1                        # Advance head position
	if(head >= numpixels):           # Off end of strip?
		head    = 0              # Reset to start
		color >>= 8              # Red->green->blue->black
		if(color == 0): color = 0xFF0000 # If black, reset to red

	tail += 1                        # Advance tail position
	if(tail >= numpixels): tail = 0  # Off end? Reset
