# from abc import ABC, abstractmethod
import os
import shutil

from nodes import Node, Expression
from visualize import generate_viz_graph, write_graph_pic


class Tree:

    def __init__(self, graph, lights, debug=True, debug_dir="debug", pause=0):
        self.graph = graph
        self.lights = lights
        self.debug = debug
        self.pause = pause
        self.debug_dir = debug_dir
        self.debug_num = 0
        if os.path.exists(debug_dir):
            shutil.rmtree(debug_dir, ignore_errors=True)
        os.mkdir(debug_dir)
        if self.debug:
            g = generate_viz_graph(self.graph)
            self.write_debug("initial", g)

    def reset(self):
        print("reset")
        # g = generate_viz_graph(self.graph)
        # self.write_debug("reset", g)
        for node in self.graph:
            print("name: " + node.name)
            node.turn_on(self.lights)
        if self.debug:
            g = generate_viz_graph(self.graph)
            self.write_debug("reset", g)

    def mutate(self):
        print("mutate")
        # g = generate_viz_graph(self.graph)
        # self.write_debug("mutate", g)
        for node in self.graph:
            if isinstance(node, Expression):
                print("name: " + node.name)
                node.mutate(self.lights)
        if self.debug:
            g = generate_viz_graph(self.graph)
            self.write_debug("mutate", g)

    def shut_down(self):
        print("shut down")
        # g = generate_viz_graph(self.graph)
        # self.write_debug("shut down", g)
        for node in self.graph:
            print("name: " + node.name)
            node.turn_off(self.lights)
        if self.debug:
            g = generate_viz_graph(self.graph)
            self.write_debug("shut down", g)

    def write_debug(self, name, g):
        write_graph_pic(self.debug_dir + '/stage-' + str(self.debug_num) + name.replace('/','-'), g)
        self.debug_num += 1

    def dfs(self, root):
        if root == None or not (isinstance(root, Node)):
            raise RuntimeError("Invalid node:" + root)

        s = [root]
        seen = set()
        while s != []:
            curr = s.pop()
            curr.heal(self.lights)
            if self.debug:
                g = generate_viz_graph(self.graph)
                self.write_debug(curr.name, g)
            edges = self.graph[curr]
            seen.add(curr)
            for n in edges:
                if n not in seen:
                    s.append(n)
