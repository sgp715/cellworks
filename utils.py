import json

from nodes import Result, Medicine, Expression, Gene


def read_json(graph_file):

    with open(graph_file) as file:
        json_str = file.read()
    data = json.loads(json_str)
    return data


def json_to_graph(data):

    graph = {}
    nodes = {}
    for node in data["nodes"]:
        node_name = node["node_name"]
        node_class = node["class"]
        addresses = node["addresses"]
        node_instance = None
        if node_class == "Result":
            node_instance = Result(node_name, addresses, node["initial"])
        elif node_class == "Medicine":
            node_instance = Medicine(node_name, addresses, node["initial"])
        elif node_class == "Expression":
            node_instance = Expression(node_name, addresses, node["initial"], node["secondary"])
        elif node_class == "Gene":
            node_instance = Gene(node_name, addresses, node["initial"])
        else:
           raise RuntimeError("Illegal json node type:" + node_class)

        graph[node_instance] = []
        nodes[node_name] = node_instance

    for key, vals in data["edges"].items():
        node = nodes[key]
        for v in vals:
            graph[node].append(nodes[v])

    return graph


if __name__ == "__main__":

    file_name = "./cellworks.json"
    data = read_json(file_name)
    graph = json_to_graph(data)
    print(graph)