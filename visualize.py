import pydot
from nodes import Medicine, Gene, Expression, Result


def generate_viz_graph(adj):

    graph = pydot.Dot(graph_type="digraph")

    for node in adj:
        n_name = node.name
        n = pydot.Node(n_name, style="filled", fillcolor=node.color)
        graph.add_node(n)
        for friend in adj[node]:
            f_name = friend.name
            edge = pydot.Edge(n_name, f_name)
            graph.add_edge(edge)

    return graph

def write_graph_pic(name, graph):

    graph.write_png(name + ".png")

if __name__ == "__main__":

    A = Medicine("A", "1")
    B = Gene("B", "2")
    C = Expression("C", "3", "blue", True)
    E = Expression("E", "5", "red", False)
    D = Result("D", "4")
    adj = {A: [B],
           B: [C, E],
           C: [D],
           D: [],
           E: []}
    g = generate_viz_graph("test", adj)
    write_graph_pic(g)
